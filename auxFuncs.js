var stripHeight=0.5,flameColor="#488EE1";

function getColor(i){
  // return 'rgb('+Math.round(150+105*(i/meterNum))+','+255+','+128+')';
  return 'rgb(12,223,255)';
}

function getColorGreen(i){
  return 'rgb('+Math.round(150+105*(i/meterNum))+','+255+','+128+')';
}

function getColorBlue(i){
  // return 'rgb('+Math.round(100+(i*20)%155)+',128,255)';
  // return 'rgb(30,180,204)';
  return "#ffffff";
}

function getColorGray(i){
  // return 'rgb(12,223,255)';
  return "#ffffff";
}

function getRGB(r,g,b){
  // return 'rgb('+Math.round(150+105*(i/meterNum))+','+255+','+128+')';
  return "rgb("+r+","+g+","+b+")";
}

function loadColor(color){
    var colorIndex;
    for(var i=0;i<colorArray.length;i++)
      if(color==colorArray[i][0])
        colorIndex=i;
    r=colorArray[colorIndex][1]/255;
    g=colorArray[colorIndex][2]/255;
    b=colorArray[colorIndex][3]/255;
}

function xToLeft(x){
  return (x+Math.round(visWidth/4));
}

function yToTop(y){
  return (y-Math.round(visWidth/1.85));
}

function getMinNote(){
  var min=melody[0][0];
  for(var i=0;i<melody.length;i++)
    if(melody[i][0]<min)
      min=melody[i][0];
  // console.log("min: "+min);
  return min;
}

function getMaxNote(){
  var max=melody[0][0];
  for(var i=0;i<melody.length;i++)
    if(melody[i][0]>max)
      max=melody[i][0];
  // console.log("max: "+max);
  return max;
}

function getYDelta(){
  return (stripHeight*height)/(range-1);
}

function getBallYFromNote(note){
  var relativeNote = Math.pow((note-minNote)/range,4);
  // var relativeNote = Math.pow((note-minNote),1);
  var footerHeight = 0.5*(1-stripHeight)*height;
  return (relativeNote*YDelta*range+footerHeight);
  // return (relativeNote*YDelta+footerHeight);
}

function getChordNoteHeight(j,currChord,lastNoteHeight,chordLen){
  // var noteDifference=Math.abs(melody[currChord][j]-melody[currChord][j-1]);
  var noteDifference=Math.abs(sortedChordNotes[j]-sortedChordNotes[j-1]);
  if(currChord==7){
    console.log("sortedChordNotes[j]: "+sortedChordNotes[j]);
    console.log("sortedChordNotes[j-1]: "+sortedChordNotes[j-1]);
  }
  if((noteDifference/range)<0.07)
    noteDifference=range*0.07;
  if((noteDifference/range)>0.15)
    noteDifference=range*0.15;
  return (lastNoteHeight+stripHeight*height*(noteDifference/range));
}

function createSortedChordArray(chordLen,currChord){
  sortedChordNotes=[];
  sortedChordNotes=new Array(chordLen);
  sortedChordNotes[0]=melody[currChord][0];
  for (var j=1;j<chordLen;j++)
    sortedChordNotes[j]=melody[currChord][chordLen-j];
}

function getX(i){
  if(i==0)
    return quartWidth;
  if (i<chordsPerPage)
    return visibleBallsPosition[i];
  return width+ballSize;
}

function auxToggleFlags(){
  for (var i=0; i<chordsPerPage;i++){
    locFlags[i] = document.createElement('div');
    locFlags[i].id = 's'+i;
    locFlags[i].className = 'middleS';
    locFlags[i].style.left = getX(i)+"px";
    notesBalls.appendChild(locFlags[i]);
  }
}

//Hides the last few balls added to the balls[] array to help the
//function shiftNotesOnScreen() run smoothly in the end of the melody.
function createHiddenFakeBalls(){
  for (var i=melody.length; i<=(melody.length+chordsPerPage);i++){
    balls[i] = document.createElement('div');
    balls[i].id = 'b'+i;
    balls[i].className = 'ballDivs';
    balls[i].style.left = width+"px";
    ballHeight=height/2;
    ballSize=50*(ballHeight/(0.8*height));
    balls[i].style.bottom  = ballHeight+"px";
    balls[i].style.width  = ballSize+"px";
    balls[i].style.height = ballSize+"px";
    balls[i].style.borderRadius  = (ballSize/2)+"px";
    notesBalls.appendChild(balls[i]);
    balls[i].style.visibility = "hidden";
  }
}

function printVelocities(){
  for(var i=0;i<velocities.length;i++){
    console.log(velocities[i]);
  }
}
