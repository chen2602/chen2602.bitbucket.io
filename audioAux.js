var sortedChordNotes=[],autoInterval;

function styleBall(i){
  balls[i].style.left = width+"px";
  ballHeight=getBallYFromNote(melody[i][0]);
  normalizedVelocity=velocities[currNoteInit]/127;
  ballSize=ballSizeInitial*Math.pow(normalizedVelocity,1);
  balls[i].style.bottom  = ballHeight+"px";
  balls[i].style.width  = ballSize+"px";
  balls[i].style.height = ballSize+"px";
  balls[i].style.borderRadius  = (ballSize/2)+"px";
  balls[i].style.opacity=0;
  balls[i].style.zIndex=600;
  balls[i].style.backgroundColor = ballColor;
}

function createChordBalls(chordFirstNote,currChord){
  var chordLen=melody[currChord].length;
  ballSize=ballSizeInitial*(55/75)*(velocities[chordFirstNote]/127);
  var chordNoteHeights=new Array(chordLen);
  chordNoteHeights[0]=0;
  createSortedChordArray(chordLen,currChord);
  for(var j=1;j<chordLen;j++){
    chordBalls[currNoteInit] = document.createElement('div');
    chordBalls[currNoteInit].id = 'bc'+currNoteInit;
    chordBalls[currNoteInit].className = 'ballDivs';
    chordNoteHeights[j]=getChordNoteHeight(j,currChord,chordNoteHeights[j-1],chordLen);
    chordBalls[currNoteInit].style.left = (ballSize/6)+"px";
    chordBalls[currNoteInit].style.top  = chordNoteHeights[j]+"px";
    chordBalls[currNoteInit].style.width  = ballSize+"px";
    chordBalls[currNoteInit].style.height = ballSize+"px";
    chordBalls[currNoteInit].style.borderRadius  = (ballSize/2)+"px";
    chordBalls[currNoteInit].style.backgroundColor = getColorGray(currChord);
    chordBalls[currNoteInit].style.opacity = 0.6*opacityOn;
    balls[currChord].appendChild(chordBalls[currNoteInit],currNoteInit);
    currNoteInit++;
  }
  if(currChord==7){
    for(var j=1;j<chordLen;j++)
      console.log("height of j="+j+" is : "+chordNoteHeights[j]);
  }
}

function autoPlay(){
  autoInterval = setInterval(function(){
    if(loadedNotes==88)
      playPosition();
  },500);
}

function getGainAndSetLowpass(){
    var normVel=1.25*Math.pow(velocities[currNote]/127,0.8);
    if(normVel>1) normVel=1;
    var chordLen = melody[curr-1].length;
    var gain=(normVel-(chordLen-1)*0.025);
    lowpass=210*gain+400;
    return gain;
}

function getGain(len){
  if(len>1)
    return 0.7;
  return 1;
}

function getLowpass(len){
  return 650;
}

function filterLowpass(freq){
  var cutoffFreq = freq;
  var lowpassNode = ctx.createBiquadFilter();
  lowpassNode.type = "lowpass";
  lowpassNode.frequency.value = cutoffFreq;
  return lowpassNode;
}

function filterGain(gain){
  var targetGain=gain;
  var gainNode = ctx.createGain();
  gainNode.gain.value = targetGain;
  return gainNode;
}

function filterCompress(thresh){
  var compressor = ctx.createDynamicsCompressor();
  compressor.threshold.value = thresh;
  compressor.knee.value = 40;
  // compressor.ratio.value = 12;
  // compressor.reduction.value = -20;
  compressor.ratio.value = 1;
  compressor.reduction.value = 0;
  compressor.attack.value = 0;
  compressor.release.value = 0.25;
  return compressor;
}
