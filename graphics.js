var gCtx,canvas;
var currWidth,currHeight;
var rect,visualizer;
var meterNum = 30;
var meters = new Array(meterNum);
var ticking = false,intervalIterations=0;
var counter = 0,step,arrayLen=256,val;
var drawInterval;
var requestAnimationFrame;
var incScale=0,x,y,visWidth = 265;
var fps,fpsInterval,startTime,now,then,elapsed;
var multiPlays=0,visFill;

function createEmptyCircle(R){
  visFill = document.getElementById("visFill");
  visFill.style.width=2*R+"px";
  visFill.style.height=2*R+"px";
  visFill.style.borderRadius=R+"px";
}

function initVisualizer(R){
  visualizer.style.width=2*R;
  visualizer.style.height=2*R;
  visualizer.style.top="49%";
  visualizer.style.transform="translateY(-49%)";
  createEmptyCircle(R);
}

function initGraphics(){
  visualizer = document.getElementById("visualizer");
  var R = Math.round(visWidth/4);
  initVisualizer(R);
  var t; //the angle tetta
  for (var i=0; i<meterNum;i++){
    meters[i] = document.createElement('div');
    meters[i].id = 'm'+i;
    meters[i].className = 'visDivs';
    t = (i/meterNum)*2*(Math.PI);
    y = R*Math.sin(t);
    x = R*Math.cos(t);
    deg = (i/meterNum)*360+90;
    meters[i].style.transform = "rotate("+deg+"deg) scaleY(0.03)";
    meters[i].style.left = xToLeft(x)+"px";
    meters[i].style.top  = yToTop(y)+"px";
    meters[i].style.backgroundColor = meterColor;
    visualizer.appendChild(meters[i]);
  }
  requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame;
  step = Math.round((arrayLen/4)/meterNum);
  step=1;
}

function draw(){
  if (multiPlays!=0)
    requestAnimationFrame(draw);
  now = Date.now();
  elapsed = now - then;
  // drawFlare();
  if(elapsed > fpsInterval ){
    then = now - (elapsed % fpsInterval);
    array = new Uint8Array(analyserFirst.frequencyBinCount);
    analyserFirst.getByteFrequencyData(array);
    for (var i=0; i<meterNum;i++){
      incScale = (array[i*step])/(2*arrayLen);
      deg = (i/meterNum)*360+90;
      meters[i].style.transform = "rotate("+deg+"deg) scaleY(" +incScale+ ")";
    }
  }
}

function resetTransitions(){
  for (var i=0; i<meterNum;i++)
    meters[i].style.transitionDuration = "0.12s";
}

function startDraw(fps){
  multiPlays++;
  fpsInterval = 1000/fps;
  then=Date.now();
  startTime=then;
  resetTransitions();
  setTimeout(clearDraw ,3000);
  draw();
}

function clearDraw(){
  multiPlays--;
  setTimeout(function(){
    if(multiPlays==0){
      for (var i=0; i<meterNum;i++){
        deg = (i/meterNum)*360+90;
        meters[i].style.transitionDuration = "0.3s";
        meters[i].style.transform = "rotate("+deg+"deg) scaleY(" +0+ ")";
        // visFill.style.opacity=0;
      }
    }
  },50);
}
