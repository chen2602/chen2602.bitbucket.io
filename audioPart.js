var decodedBuffer,getSound,analyser,analyserFirst;
var notesPaths = new Array(88);
var notes = new Array(88);
var lines,melody = [], velocities = [];
var curr=0; // Current note in melody
var capVal=0,noteId=1;
var loadedNotes=0,lowpass=560;
var currNote=0,firstNote=true;

//TODO: Fix the too-low velocity issue in the last notes of the melody
window.onload = function() {
  init();
  initGraphics();
  loadNoteFile();
};

function playPosition(){
  var chord = melody[curr];
  var len = chord.length;
  var timestampDelta,chordNote=1;
  shiftNotesOnScreen();
  startDraw(20);
  var colorNote=255*((chord[0]-minNote)/range);
  var colorVelocity=255*(velocities[currNote]/127);
  curr++;
  notes[chord[0]-21].play(true);
  currNote++;
  timestampDelta=(15*Math.random()+25);
  var playChordNote = function(){
      clearInterval(interval);
      notes[chord[chordNote]-21].play(false);
      currNote++;
      chordNote++;
      if(chordNote<len)
        interval = setInterval(playChordNote,timestampDelta);
  }
  if(len>1){
    var currNoteInChord=0;
    var interval = setInterval(playChordNote, timestampDelta);
  }
  if(curr>=melody.length)
    window.onkeypress = function(e){};
}

function init(){
  window.AudioContext = window.AudioContext || window.webkitAudioContext;
  ctx = new AudioContext();
  decodedBuffer,getSound;
  for (var i=0; i<88; i++){
    notesPaths[i]="./Cleaned/"+Number(i+1)+".mp3";
    notes[i]=new Object;
    addAudioProperties(notes[i],notesPaths[i],notesPaths[i]);
  }
  console.log("done init()");
}

function loadAudio(object,url){
  var request = new XMLHttpRequest();
  request.open('GET', url, true);
  request.responseType = 'arraybuffer';
  request.onload = function() {
      ctx.decodeAudioData(request.response, function(buffer) {
          object.buffer = buffer;
          loadedNotes++; //holds the number of notes already loaded to memory
      });
  }
  request.send();
}

function loadNoteFile(){
  var request = new XMLHttpRequest();
  var allNotes;
  request.open('GET', "./Midis/MorningMood7Out", true);
  request.onload = function() {
    var data = request.response;
    var timestamp,note,velocity,k=0,chordIndex=0;
    allNotes = data.split("\n");
    var firstLine=allNotes[0].split(",");
    var minAll=firstLine[1];
    var maxAll=minAll;
    for (var j=0; j<allNotes.length;j++)
      allNotes[j] = allNotes[j].split(",");
    while(k<allNotes.length-1){
      timestamp = allNotes[k][0];
      note      = allNotes[k][1];
      velocity  = allNotes[k][2];
      var chord=[];
      chord.push(note);
      velocities.push(velocity);
      while((timestamp > (allNotes[k+1][0]-32)) && (k+1)<(allNotes.length-1)){
        k++;
        timestamp = allNotes[k][0];
        note      = allNotes[k][1];
        velocity  = allNotes[k][2];
        chord.push(note);
        velocities.push(velocity);
        if(k==allNotes.length-1) break;
      }
      melody.push(chord);
      chordIndex++;
      k++;
    }
    allNotesLen=allNotes.length;
    initBackg();
    hideLoadScreen();
  }
  request.send();
}

function addAudioProperties(object,name,path) {
    object.name = name;
    object.source = path;
    loadAudio(object, object.source);
    object.play = function (isFirst) {
        var sourceNode = ctx.createBufferSource(); //each play you create a new buffer source!
        sourceNode.buffer = object.buffer;
        var lowpassNode = filterLowpass(lowpass);
        var normalizedGain=getGainAndSetLowpass();
        var gainNode = filterGain(normalizedGain);
        if(isFirst){
          analyserFirst = ctx.createAnalyser();
          analyserFirst.fftSize=2*arrayLen;
          sourceNode.connect(analyserFirst);
          analyserFirst.connect(lowpassNode);
        }else{
          analyser = ctx.createAnalyser();
          analyser.fftSize=2*arrayLen;
          sourceNode.connect(analyser);
          analyser.connect(lowpassNode);
        }
        lowpassNode.connect(gainNode);
        gainNode.connect(ctx.destination);
        sourceNode.start(0);
        object.s = sourceNode;
    }
}

window.onkeypress = function(e) {
  if(firstNote==true) {hidePieceName(); firstNote=false;}
  if(loadedNotes==88)
    playPosition();
};
