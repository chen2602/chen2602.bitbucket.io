var requestID,notesBalls,ballsPosition=0;
var ballsNum,width,chordsPerPage=11,R=20;
var ballsFakeSize;
var balls = new Array(ballsFakeSize),ballsX = new Array(ballsNum);
var ballsSpaces = new Array(chordsPerPage);
var ballsStep, visibleBallsPosition = new Array(chordsPerPage);
var quartWidth;
var locFlags = new Array(chordsPerPage);
var ballSizeInitial=60,minNote,maxNote,range,YDelta,bodyStyle;
var chordBalls=[],currNoteInit=0, opacityOn=1;
var heights = new Array(ballsNum); //TODO: Delete this Array
var ballColor='#2871f3',backgroundColor= '#174087',meterColor='white'; //'#01fe71'

function initBackg(){
  width   = document.body.clientWidth;
  height  = document.body.clientHeight;
  minNote=getMinNote(); // The melody is already loaded to memory in this stage.
  maxNote=getMaxNote();
  range=maxNote-minNote;
  YDelta=getYDelta();
  bodyStyle=document.body.style;
  bodyStyle.backgroundColor = backgroundColor;
  quartWidth = 0.25 * width;
  createBalls();
  console.log("done initBackg()");
}

function createBalls(){
  chordsNum=melody.length;
  ballsFakeSize=chordsNum+chordsPerPage+2;
  notesBalls = document.getElementById("notesBalls");
  createSpaces();
  var ballHeight=0,ballSize,normalizedVelocity;
  for (var i=0; i<chordsNum;i++){
    balls[i] = document.createElement('div');
    balls[i].id = 'b'+i;
    balls[i].className = 'ballDivs';
    styleBall(i);
    currNoteInit++;
    notesBalls.appendChild(balls[i],currNoteInit);
  }
  positionFirstBalls();
  createHiddenFakeBalls();
  notesBalls.style.zIndex=500;
}

function positionFirstBalls(){
  for(var i=0;i<chordsPerPage;i++){
    balls[i].style.transform = "translateX("+(-1)*(width-visibleBallsPosition[i])+"px)";
    balls[i].style.opacity = opacityOn;
    balls[i].style.zIndex=600;
  }
}

function createSpaces(){
  var m=chordsPerPage-1; //number of spaces
  var d=2*(12/16)*width/(3*m*(m-1)); //the increment in spaces
  var s1=(2*(12/16)*width)/(3*m); //the first space
  visibleBallsPosition[0]=quartWidth;
  for (var i=0; i<m; i++){
    ballsSpaces[i]=s1+(m-i-1)*d;
    visibleBallsPosition[i+1]=visibleBallsPosition[i]+ballsSpaces[i];
  }
  var sum=0;
  for(var i=0;i<m;i++)
    sum+=ballsSpaces[i];
  ballsStep = s1;
}

function shiftNotesOnScreen(){
  balls[curr].style.opacity = 0;
  balls[curr].style.transform = "translateX("+(-1*(width-visibleBallsPosition[0]+ballSize))+"px)";
  balls[curr+chordsPerPage-1].style.opacity=1;
  for(var i=1;i<chordsPerPage;i++)
    balls[curr+i].style.transform = "translateX("+(-1)*(width-visibleBallsPosition[i-1])+"px)";
  balls[curr+i].style.opacity = opacityOn;
}

function hideLoadScreen(){
  var loading = document.getElementById('loading');
  loading.style.opacity=0;
  loading.style.visibility='hidden';
}

function hidePieceName(){
  var pieceName = document.getElementById('pieceName');
  var pressAny = document.getElementById('pressAny');
  var about = document.getElementById('about');
  pieceName.style.opacity=0;
  pieceName.style.visibility='hidden';
  pressAny.style.opacity=0;
  pressAny.style.visibility='hidden';
  about.style.opacity=0.2;
}
